make packer-build
# This will take about 5 minutes. Copy the Snapshot ID you get at the end

make terraform-init
# Only have to do this once

make terraform-plan
<enter Snapshot ID from packer-build command>

# If all looks good, then:

make terraform-apply
<enter Snapshot ID from packer-build command>
# This will take about a minute
